import os.path

import skimage
import skimage.color
import skimage.data
import skimage.exposure
import skimage.io
import skimage.transform

import fabemd


def export_fabemd(image, directory):
    if not os.path.exists(directory):
        os.mkdir(directory)
    skimage.io.imsave(os.path.join(directory, "input.png"), image)
    with open(os.path.join(directory, "scales.tsv"), "w") as scale_file:
        for index, mode in enumerate(fabemd.decompose(image)):
            scale_file.write("{0}\t{1}\n".format(index, mode.scale))
            mode_image = skimage.exposure.rescale_intensity(mode.image, out_range=(0.0, 1.0))
            mode_image_path = os.path.join(directory, "mode-{}.png".format(index))
            skimage.io.imsave(mode_image_path, mode_image)


if __name__ == "__main__":
    image = skimage.img_as_float(skimage.color.rgb2grey(skimage.data.lena()))
    export_fabemd(image, "original")
    image = skimage.transform.rescale(image, 1.5, mode="reflect")
    export_fabemd(image, "enlarged")
