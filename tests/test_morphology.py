import numpy
import skimage
import skimage.color
import skimage.data
import skimage.io

from fabemd import morphology


if __name__ == "__main__":
    image = skimage.img_as_float(skimage.color.rgb2grey(skimage.data.lena()))
    skimage.io.imsave("input.png", image)
    skimage.io.imsave("dilated.png", morphology.dilate(image, 13))
    skimage.io.imsave("eroded.png", morphology.erode(image, 13))
    skimage.io.imsave("xdilated.png", morphology.xdilate(image, 13))
    skimage.io.imsave("xeroded.png", morphology.xerode(image, 13))
    maxima = morphology.local_maxima(image, 13)
    skimage.io.imsave("maxima.png", morphology.draw(image.shape, maxima))
    minima = morphology.local_minima(image, 13)
    skimage.io.imsave("minima.png", morphology.draw(image.shape, minima))
