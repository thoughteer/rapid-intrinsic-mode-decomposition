import collections

import numpy
import numpy.fft
import scipy.ndimage.fourier
import scipy.spatial.distance

from fabemd import morphology


Mode = collections.namedtuple("Mode", ["image", "scale"])


def calculate_spread(points):
    """
    Calculate the minimum distance between distinct points from the given list.
    """
    return scipy.spatial.distance.pdist(points, metric="chebyshev").min()


def box_filter(image, radius):
    """
    Perform local averaging with a square window of the given @radius.
    """
    size = 2 * radius + 1
    return scipy.ndimage.filters.uniform_filter(image, size=size)


def iir_smoother(image, sigma):
    """
    Apply IIR smoothing to the @image with the given SD @sigma.
    """
    spectrum = numpy.fft.fft2(image)
    modulated_spectrum = scipy.ndimage.fourier.fourier_gaussian(spectrum, sigma)
    return numpy.fft.ifft2(modulated_spectrum).real


def extract_mode(image, scale):
    """
    Extract an intrinsic mode from the @image at the given @scale.

    Returns a tuple (intrinsic_mode, residue, associated_scale).
    """
    # smooth a bit to avoid dealing with plateau regions
    image = iir_smoother(image, 0.01)
    # find local extrema
    maxima = morphology.local_maxima(image, scale)
    minima = morphology.local_minima(image, scale)
    # find the smallest distance between the extrema
    spread = min(calculate_spread(maxima), calculate_spread(minima))
    # find a corresponding radius (gonna be the next scale)
    radius = (spread + 1) // 2
    # calculate the mean
    upper_envelope = morphology.dilate(image, radius)
    lower_envelope = morphology.erode(image, radius)
    mean = box_filter((upper_envelope + lower_envelope) / 2, radius)
    # decompose the image
    return image - mean, mean, radius


def decomposable(image, scale):
    """
    Determine whether the @image can be decomposed further from the @scale.
    """
    image = iir_smoother(image, 0.01)
    maximum_count = morphology.local_maxima(image, scale).shape[0]
    minimum_count = morphology.local_minima(image, scale).shape[0]
    return maximum_count >= 2 and minimum_count >= 2


def decompose(image):
    """
    Generate a FABEMD of the @image as a sequence of modes.
    """
    residue = image
    minimal_scale = 1
    while decomposable(residue, minimal_scale):
        mode, residue, scale = extract_mode(residue, minimal_scale)
        yield Mode(mode, scale)
        minimal_scale = max(scale, minimal_scale + 1)
    yield Mode(residue, numpy.Infinity)
