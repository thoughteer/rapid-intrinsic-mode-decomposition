import functools
import operator

import numpy
import scipy
import scipy.ndimage


def line(radius):
    """
    Make a (horizontal) line structuring element (SE) of size (2 * @radius + 1).
    """
    size = 2 * radius + 1
    return numpy.ones((1, size))


def punctured_line(radius):
    """
    Make a (horizontal) line SE of size (2 * @radius + 1) punctured at the center.
    """
    result = line(radius)
    result[0, radius] = 0
    return result


def rank_filter(image, footprint, proxy):
    """
    Apply @proxy (rank filter) to the @image with the given @footprint.
    """
    return proxy(image, footprint=footprint, mode="reflect")


def separable_rank_filter(image, footprints, proxy):
    """
    Apply @proxy (rank filter) to the @image in a separable manner with the given @footprints.
    """
    buffer = rank_filter(image, footprints[0], proxy)
    return rank_filter(buffer.T, footprints[1], proxy).T


def full_rank_filter(image, radius, proxy):
    """
    Perform rank filtering of the @image with a square window of the given @radius.
    """
    return separable_rank_filter(image, (line(radius),) * 2, proxy)

dilate = functools.partial(full_rank_filter, proxy=scipy.ndimage.maximum_filter)

erode = functools.partial(full_rank_filter, proxy=scipy.ndimage.minimum_filter)


def punctured_rank_filter(image, radius, proxy, fuser):
    """
    Perform rank filtering of the @image with a punctured square window of the given @radius.
    """
    footprints = (line(radius), punctured_line(radius))
    horizontal_buffer = separable_rank_filter(image, footprints, proxy)
    vertical_buffer = separable_rank_filter(image, footprints[::-1], proxy)
    return fuser(horizontal_buffer, vertical_buffer)

xdilate = functools.partial(punctured_rank_filter,
                            proxy=scipy.ndimage.maximum_filter,
                            fuser=numpy.maximum)

xerode = functools.partial(punctured_rank_filter,
                           proxy=scipy.ndimage.minimum_filter,
                           fuser=numpy.minimum)


def local_extrema(image, radius, xfilter, comparator):
    """
    Get a list of local extrema coordinates.
    """
    xfiltered_image = xfilter(image, radius)
    peak_mask = comparator(image, xfiltered_image)
    return numpy.vstack(peak_mask.nonzero()).T

local_maxima = functools.partial(local_extrema,
                                 xfilter=xdilate,
                                 comparator=operator.gt)

local_minima = functools.partial(local_extrema,
                                 xfilter=xerode,
                                 comparator=operator.lt)


def draw(shape, points):
    """
    Render @points onto an image of the given @shape.
    """
    result = numpy.zeros(shape)
    result[tuple(points.T)] = 1.0
    return result
